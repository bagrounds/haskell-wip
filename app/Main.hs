module Main where

import Lispish
import System.IO

flushStr :: String -> IO ()
flushStr str = putStr str >> hFlush stdout

main :: IO ()
main = do
  flushStr "> "
  line <- getLine
  _ <- putStrLn ("Result   => " ++ (interpret eval line))
  _ <- putStrLn ("Node count: " ++ (interpret count line))
  _ <- putStrLn ("Depth     : " ++ (interpret depth line))
  main
