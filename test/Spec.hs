import Test.Framework (defaultMain, testGroup, Test)
import Test.Framework.Providers.QuickCheck2 (testProperty)

import Test.QuickCheck
import Parser
import Data.List

main :: IO ()
main = defaultMain tests

tests :: [Test]
tests =
  [ testProperty "parseChar" parseChar
  , testProperty "sepByIntersperse" sepByIntersperse
  ]

sepByIntersperse :: String -> Char -> Bool
sepByIntersperse x sep = let
  str = (intersperse sep x)
  parser = parse (item `sepby` char sep) in
  (parser str) == [(x, "")]

parseChar :: String -> Bool
parseChar x = case x of
  (c:cs) -> parse (char c) x == [(c, cs)]
  "" -> True

