{-# LANGUAGE TupleSections #-}

module Parser where

import Control.Monad ( liftM2, ap, MonadPlus(..))
import Control.Applicative (liftA2, Alternative(..))
import Control.Arrow (first)
import Data.Char (isSpace)

data Parser c t a = Parser { parse :: c t -> [(a, c t)] }

instance Functor (Parser c t) where
  fmap f (Parser p) = Parser (fmap (first f) . p)

instance Applicative (Parser c t) where
  pure  = return
  (<*>) = ap

instance Monad (Parser c t) where
  return a = Parser $ (: []) . (a ,)
  p >>= f = Parser (concat . fmap g . parse p) where
    g = uncurry parse . first f

instance MonadPlus (Parser c t) where
  mzero = Parser $ const []
  mplus p q = Parser $ liftM2 (++) (parse p) (parse q)

instance Alternative (Parser c t) where
  empty = mzero
  p <|> q = Parser $ \s ->
    case parse p s of
      [] -> parse q s
      x -> x

instance Monoid (Parser c t a) where
  mempty = empty
  mappend = mplus

item :: Parser [] t t
item = Parser $ uncons where
  uncons [] = []
  uncons (c:cs) = [(c, cs)]

sat :: (t -> Bool) -> Parser [] t t
sat p = item >>= f where
  f c = if p c then return c else empty

isNot :: (t -> Bool) -> Parser [] t t
isNot = sat . (not .)

oneOf :: Eq t => [t] -> Parser [] t t
oneOf = sat . flip elem

notChar :: Eq t => t -> Parser [] t t
notChar = isNot . (==)

char :: Eq t => t -> Parser [] t t
char = sat . (==)

string :: Eq t => [t] -> Parser [] t [t]
string [] = return []
string (c:cs) = liftM2 (:) (char c) (string cs)

many1 :: Parser c t a -> Parser c t [a]
many1 p = liftA2 (:) p (many p)

sepby :: Parser c t a -> Parser c t b -> Parser c t [a]
p `sepby` sep = (p `sepby1` sep) <|> return []

sepby1 :: Parser c t a -> Parser c t b -> Parser c t [a]
p `sepby1` sep = liftM2 (:) p (many (sep >> p))

chainl :: Parser c t a -> Parser c t (a -> a -> a) -> a -> Parser c t a
chainl p op a = (p `chainl1` op) <|> return a

chainl1 :: Parser c t a -> Parser c t (a -> a -> a) -> Parser c t a
p `chainl1` op = p >>= rest where
  rest a = (liftM2 ($ a) op p) <|> return a

wrapped :: Eq t => t -> Parser [] t a -> Parser [] t a
wrapped x = wrapped2 x x

wrapped2 :: Eq t => t -> t -> Parser [] t a -> Parser [] t a
wrapped2 t1 t2 x = (char t1) *> x <* (char t2)

-- ParserString

space :: Parser [] Char String
space = many $ sat isSpace

token :: Parser [] Char a -> Parser [] Char a
token = flip (liftM2 const) space

symb :: String -> Parser [] Char String
symb = token . string

apply :: Parser [] Char a -> String -> [(a, String)]
apply = parse . (space >>)

list :: Parser [] Char a -> Parser [] Char [a]
list = parens . flip sepby (char ' ')

quotes :: Parser [] Char a -> Parser [] Char a
quotes = wrapped '"'

parens :: Parser [] Char a -> Parser [] Char a
parens = wrapped2 '(' ')'
