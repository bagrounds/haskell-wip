{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TupleSections #-}

module ArrowParser
    ( AParser
    , aParse
    , aSymbol
    , aItem
    , aSat
    ) where

import Control.Arrow hiding ((+++))
import Control.Category
import Data.List
import Data.Char

data StaticParser s = SP Bool [s]
newtype DynamicParser s a b = DP ((a, [s]) -> (b, [s]))
data AParser s a b = P (StaticParser s) (DynamicParser s a b)

instance Eq s => Arrow (AParser s) where
  arr f = P (SP True []) (DP (\(b, s) -> (f b, s)))
  first (P sp (DP p)) =
    P sp $ DP (\((b, d), s) -> let (c, s') = p (b, s) in ((c, d), s'))

instance Eq s => Category (AParser s) where
  id = arr Prelude.id
  P (SP empty1 starters1) (DP p1) . P (SP empty2 starters2) (DP p2) =
    P (SP (empty1 && empty2)
       (starters1 `union` (if empty1 then starters2 else [])))
    (DP (p2 >>> p1))

aItem :: AParser s [s] [s]
aItem = P (SP False []) (DP f) where
  f (as, []) = (as, [])
  f (as, (b:bs)) = (b : as, bs)

aSat :: Eq s => (s -> Bool) -> AParser s [s] [s]
aSat p = aItem >>> P (SP True []) (DP f) where
  f (as, []) = (as, [])
  f (as, (b:bs)) | p b = (b : as, bs)
                 | otherwise = (as, b:bs)

aSymbol :: Eq s => s -> AParser s [s] [s]
aSymbol c = P (SP True []) (DP f) where
  f (as, (b:bs)) | b == c = (b : as, bs)
                 | otherwise = ([], bs)

aParse :: Eq s => AParser s [s] [s] -> [s] -> ([s], [s])
aParse (P (SP b cs) (DP f)) xs = f (cs, xs)
