{-# LANGUAGE DeriveFunctor #-}
module Lispish where

import Parser
import Text.PrettyPrint (Doc, render)
import qualified Text.PrettyPrint as P
import Data.Char (isDigit, isLetter, isAlphaNum)
import Control.Applicative ((<|>), many)
import Control.Arrow
import Control.Monad

newtype Term f = In { out :: f (Term f) }

data S a
  = ListS [a]
  | Number Int
  | Name String
  | StringS String
  deriving (Functor)

s :: Parser [] Char (Term S)
s = sString <|> sNum <|> sName <|> sList s

sNum :: Parser [] Char (Term S)
sNum = In . Number . read <$> many1 (sat isDigit)

sName :: Parser [] Char (Term S)
sName = In . Name <$> liftM2 (:) (sat isLetter) (many (sat isAlphaNum))

sString :: Parser [] Char (Term S)
sString = In . StringS <$> quotes (many (notChar '"'))

sList :: Parser [] Char (Term S) -> Parser [] Char (Term S)
sList = fmap (In . ListS) . list

runParser :: Parser [] Char (Term S) -> String -> Either String (Term S)
runParser parser str =
  case parse parser str of
    [(ast, [])] -> Right ast
    x   -> Left $ "Parsing error: " ++
                 show (fmap (first (cata prettyPrint)) x)

-- recursion schemes
type Algebra f a = f a -> a

cata :: (Functor f) => Algebra f a -> Term f -> a
cata f = out >>> fmap (cata f) >>> f

bottomUp :: Functor a => (Term a -> Term a) -> Term a -> Term a
bottomUp fn = out >>> fmap (bottomUp fn) >>> In >>> fn

topDown :: Functor a => (Term a -> Term a) -> Term a -> Term a
topDown fn = In <<< fmap (bottomUp fn) <<< out <<< fn

-- interpreters
interpret :: (Term S -> String) -> String -> String
interpret f = either show f . (runParser s)

eval, count, depth :: Term S -> String
eval = render . cata prettyPrint . eval' . bottomUp eval'
count = show . cata count'
depth = show . cata depth'

eval' :: (Term S) -> (Term S)
eval' (In (ListS [In (Name "i"), x])) = x
eval' (In (ListS [In (Name "k"), x, _])) = x
eval' (In (ListS [In (Name "s"), x, y, z])) = In (ListS [x, z, In (ListS [y, z])])
eval' x = x

count' :: Algebra S Int
count' (ListS xs) = sum xs + 1
count' (Name _) = 1
count' (Number _) = 1
count' (StringS _) = 1

depth' :: Algebra S Int
depth' (Name _) = 1
depth' (Number _) = 1
depth' (StringS _) = 1
depth' (ListS []) = 1
depth' (ListS xs) = maximum xs + 1

prettyPrint :: Algebra S Doc
prettyPrint (ListS xs) = P.parens (P.cat (P.punctuate (P.text " ") xs))
prettyPrint (Number x) = P.int x
prettyPrint (Name x) = P.text x
prettyPrint (StringS x) = P.text (wrap1 "\"" x)

wrap1 :: [a] -> [a] -> [a]
wrap1 a = wrap2 a a

wrap2 :: [a] -> [a] -> [a] -> [a]
wrap2 a b str = a ++ str ++ b

